﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    private static int _nextNextLevelIndex = 1;
    private Enemy[] _enemies;
    private void OnEnable()
    {
         _enemies = FindObjectsOfType<Enemy>();
    }
    // Update is called once per frame
    void Update()
    {
        foreach(Enemy enemy in _enemies)
        {
            if(enemy != null)
            {
                return;
            }
            //Debug.Log("You killed all enemies !!!");
            _nextNextLevelIndex++;
            string nextLevelName = "Level" + _nextNextLevelIndex;
            
            SceneManager.LoadScene(nextLevelName);

        }
    }
}
